package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import automobiles.Car;

class CarTests {

	@Test
	void testCarValidation() {
		try {
			Car c = new Car(-5);
			fail("The Car constructor was supposed to throw an exception but it did not");
		}
		catch(IllegalArgumentException e) {
			
		}
		catch(Exception e) {
			fail("The Car constructor has caught an exception but it is not the good one");
		}
	}
	
	@Test
	void testGetSpeed() {
		Car c = new Car (9);
		assertEquals(9,c.getSpeed());
	}
	
	@Test
	void testGetLocation() {
		Car c = new Car (4);
		assertEquals(50,Math.floor(c.getLocation()));
	}
	
	@Test
	void testMoveRight() {
		Car c = new Car(11);
		c.moveRight();
		assertEquals(61,c.getLocation());
	}
	
	@Test
	void testMoveRight2() {
		Car c = new Car(70);
		c.moveRight();
		assertEquals(Car.MAX_POSITION,c.getLocation());
	}
	
	@Test
	void testMoveLeft() {
		Car c = new Car(7);
		c.moveLeft();
		assertEquals(43,c.getLocation());
	}
	
	@Test
	void testMoveLeft2() {
		Car c = new Car(75);
		c.moveLeft();
		assertEquals(0,c.getLocation());
	}
	
	@Test
	void testAccelerate() {
		Car c = new Car(5);
		c.accelerate();
		assertEquals(6,c.getSpeed());
	}
	
	@Test
	void testStop() {
		Car c = new Car(13);
		c.stop();
		assertEquals(0,c.getSpeed());
	}

}
