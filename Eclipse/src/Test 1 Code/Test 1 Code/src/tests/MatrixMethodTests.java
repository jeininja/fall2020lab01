package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class MatrixMethodTests {

	@Test
	void testDuplicate() {
		int[][] baseMatrix = {{1,2,3},{4,5,6}};
		int[][] resultMatrix = {{1,1,2,2,3,3},{4,4,5,5,6,6}};
		assertArrayEquals(resultMatrix, duplicate(baseMatrix));
	}

}
