package utilities;

public class MatrixMethod {
	
	public int[][] duplicate(int[][] matrix) {
		int[][] new2dArray = new int[matrix.length][];
		
		for(int i =0; i<matrix[i].length; i++) {
			for(int j=0; j<matrix[i][j];j++) {
				new2dArray[i][j]=matrix[i][j];
				new2dArray[i][j+1]=matrix[i][j];
			}
		}
		return new2dArray;
	}
}
